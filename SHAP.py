import os
#os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
import os.path
import shap
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import uproot
import tensorflow as tf
tf.random.set_seed(123)


print('Using tf version {}'.format(tf.__version__))
print('run source /cvmfs/sft.cern.ch/lcg/views/LCG_105_cuda/x86_64-el9-gcc11-opt/setup.sh if tf was done with it')

path = '/afs/cern.ch/work/e/eballabe/public/machine-learning/output_withDNN'

saved_model_A = '/afs/cern.ch/work/e/eballabe/public/machine-learning/DNN_A.keras'
saved_model_B = '/afs/cern.ch/work/e/eballabe/public/machine-learning/DNN_B.keras'

features = ['trackPt', 'trackEta', 'trackPhi', 'trackD0', 'trackD0Sig', 'trackZ0SinTheta', 'trackFitQuality', 'trackNIBLHits', 'trackPixHits','trkNonSignalPhotonConvPtcone30', 'trackMt', 'trackPixeldEdx', 'trackTRTdEdx'] # input features 
branches = ['mu', 'EventNumber', 'RunNumber', 'dsid', 'genWeight', 'eventWeight', 'leptonWeight', 'photonWeight', 'JVTWeight', 'pileupWeight', 'trigWeight', 'trigWeight_singleElectronTrig', 'trigWeight_singleMuonTrig', 'trigWeight_singlePhotonTrig', 'trigMatch_metTrig', 'trigMatch_singleElectronTrig', 'trigMatch_singleMuonTrig', 'trigMatch_singlePhotonTrig', 'mll', 'nJet', 'leadJetPt', 'leadJetEta', 'leadJetPhi', 'leadJetM', 'MET', 'MET_invis_electrons', 'MET_invis_muons', 'MET_invis_photons', 'MET_invis_leptons', 'MET_Phi', 'MET_invis_electrons_Phi', 'MET_invis_muons_Phi', 'MET_invis_photons_Phi', 'MET_invis_leptons_Phi', 'MET_Signif', 'MET_invis_electrons_Signif', 'MET_invis_muons_Signif', 'MET_invis_photons_Signif', 'MET_invis_leptons_Signif', 'MET_minDPhiJetMET', 'MET_invis_electrons_minDPhiJetMET', 'MET_invis_muons_minDPhiJetMET', 'MET_invis_photons_minDPhiJetMET', 'MET_invis_leptons_minDPhiJetMET', 'trackPt', 'trackEta', 'trackPhi', 'trackZ0', 'trackD0', 'trackD0Sig', 'trackZ0SinTheta', 'trackQ', 'trackFitQuality', 'trackOrigin', 'trackType', 'trackLabel', 'trackPdgId', 'trackParPdgId', 'trackNIBLHits', 'trackPixHits', 'trackIsSecTrk', 'trackMatchedToVSI', 'trkNonBaseAssocPtcone20', 'trkNonBaseAssocPtcone30', 'trkNonBaseAssocPtcone40', 'trkNonBasePhotonConvPtcone20', 'trkNonBasePhotonConvPtcone30', 'trkNonBasePhotonConvPtcone40', 'trkNonSignalAssocPtcone20', 'trkNonSignalAssocPtcone30', 'trkNonSignalAssocPtcone40', 'trkNonSignalPhotonConvPtcone20', 'trkNonSignalPhotonConvPtcone30', 'trkNonSignalPhotonConvPtcone40','trackMt', 'trackPixeldEdx', 'trackTRTdEdx'] # branches to save in the ntuples

weighted_training = False # True or False
lumi = 138950 # only used if weighted_training is True

preselection = "RunNumber>0"

train_over_signal_not_truth_matched = True # True or False. If True, train over all signal tracks. If False, train over all SUSY truth-matched signal tracks.
label_signal_not_truth_matched = '1'  # If train_over_signal_not_truth_matched = True, '0' or '1'

def read_samples(samples_to_read = 'training', remove_signal_not_truth_matched = False, label_signal_not_truth_matched = '0'):

    print('Using samples in {} : {}'.format(path, os.listdir(path)))

    ####################################################
    if samples_to_read == 'training':
        print("Reading training samples")

        higgsino_176_175p5_175_file = uproot.open(path+'/176_175p5_175.root:176_175p5_175;1')
        #Znunu_file                  = uproot.open(path+'/Znunu.root:Znunu;1')

        #branches = higgsino_175_175p5_175_file.keys()

        df_176_175p5_175 = higgsino_176_175p5_175_file.arrays(branches,preselection,library="pd")
        #df_Znunu         = Znunu_file.arrays(branches,preselection,library="pd")

        df_176_175p5_175['Sample'] = '176_175p5_175'
        #df_Znunu['Sample']         = 'Znunu'


        print(df_176_175p5_175)
        #print(df_Znunu)

        if remove_signal_not_truth_matched == True:
            print("remove_signal_not_truth_matched == True. Removing signal not truth matched tracks.")
            df_176_175p5_175.drop(df_176_175p5_175[(df_176_175p5_175['trkparpdgId'] != 1000023)  & (df_176_175p5_175['trkparpdgId'] != 1000024)].index, inplace=True)
            #df_150p5_150p5_150.drop(df_150p5_150p5_150[(df['trkparpdgId'] != 1000023)  & (df['trkparpdgId'] != 1000024)].index, inplace=True)
            #df_150p7_150p35_150.drop(df_150p7_150p35_150[(df['trkparpdgId'] != 1000023)  & (df['trkparpdgId'] != 1000024)].index, inplace=True)

        #frames = [df_171_170p5_170, df_150p5_150p5_150, df_150p7_150p35_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]
        #frames = [df_171_170p5_170, df_Znunu]
        df=df_176_175p5_175
        #df = pd.concat(frames)
    ####################################################

    elif samples_to_read == 'other':
        print("Reading other samples")
        higgsino_151_151_150_file = uproot.open(path+'/signals_by_process/N2C1p.root:tree_NoSys;1')

        #branches = higgsino_151_151_150_file.keys()
        df_151_151_150 = higgsino_151_151_150_file.arrays(branches,preselection,library="pd")
        df_151_151_150['Sample'] = '151_150p5_150'
        df = df_151_151_150

    ####################################################

    # Adding branch weight for weighted training

    if weighted_training == True:
        print("weighted_training set to True. Adding the \"weight\" column with \"weight\"=eventWeight*xsec*138950.")
        df["weight"]= df["eventWeight"]*df["xsec"]*lumi
    else:
        print("weighted_training set to False. Not considering weights in the training.")

    # Defining Ratio
    # df['RatioJetPtMET']= np.full( len(df.index), -1)
    # df['RatioJetPtMET']= df['jetPt']/df['met_Et']


    # Track labelling
    df['Label'] = np.full( len(df.index), -1)
    if (train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='0') or (train_over_signal_not_truth_matched == False):
        df.loc[ (df['trackParPdgId'] != 1000023) & (df['trackParPdgId'] != 1000024), 'Label'] = 0
        df.loc[ (df['trackParPdgId'] == 1000023) | (df['trackParPdgId'] == 1000024), 'Label' ] = 1 
    elif train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='1':
        df.loc[ (df['Sample'] != '171_170p5_170') & (df['Sample'] != '150p5_150p5_150') & (df['Sample'] != '150p7_150p35_150'), 'Label'] = 0
        df.loc[ (df['Sample'] == '171_170p5_170') | (df['Sample'] == '150p5_150p5_150') | (df['Sample'] == '150p7_150p35_150'), 'Label'] = 1

    if (  len(df[df['Label'] == -1].index)  != 0 ):
        raise Exception('Errors in associating track labels')
    elif (  len(df[df['Label'] == -1].index)  == 0 ):
        print('All tracks have been labeled')
        print('Labelled 1 tracks = {}, Labelled 0 tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))

    # Dataframe splitting according to EventNumber
    A = df[df['EventNumber']%2 == 0]
    B = df[df['EventNumber']%2 == 1]

    print('Labelled 1 tracks in A = {}, Labelled 0 tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
    print('Labelled 1 tracks in B = {}, Labelled 0 tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))

    print('Dataframe')
    print(df)
    print('B')
    print(B)
    return(df,A,B)

number_of_shap_values = 100

def main():

    df,A,B = read_samples('training' , False, '0')
    print('A',A)
    print('B',B)
    # Load the TensorFlow model
    model_A = tf.keras.models.load_model(saved_model_A)
    A_train = A[features].iloc[:number_of_shap_values]
    A_test = B[features].iloc[:number_of_shap_values]
    # Prepare your data
    # Assuming X_test is your test data
    A_train = A_train.values
    A_test = A_test.values

    print('here1',A_train.shape)
    print('here2',A_test.shape)
    
    # Calculate SHAP values
    #explainer = shap.DeepExplainer(model_A, A_test)  # using a subset of data for speed
    #shap_values = explainer.shap_values(np.asarray(A_test))
    explainer = shap.KernelExplainer(model_A, A_train)
    #explainer.expected_value = explainer.expected_value[0].squeeze()  # Additional line to force the dim of base_values

    shap_values = explainer.shap_values(A_train)
    
    print("Shape of shap_values:", np.squeeze(shap_values).shape)
    print("Shape of A_test:", A_test.shape)
    print("Shape of features:", features)
        
    
    # Plot SHAP values
    #fig=plt.gcf()
    #shap.summary_plot(np.squeeze(np.asarray(shap_values)), np.asarray(A_test), np.asarray(features), plot_type="bar")
    shap.summary_plot(np.squeeze(shap_values), A_test, features, show=False)
    plt.savefig('mygraph.pdf', format='pdf', dpi=600, bbox_inches='tight')
    #fig.savefig('shap_summary_plot.png')  # Save the plot as an image

if __name__ == "__main__":
    main()
