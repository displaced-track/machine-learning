import uproot
import os
#os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path
import datetime
    
print('Using tf version {}'.format(tf.__version__))

path = '/eos/user/e/eballabe/samples-with-tracks'

model_name = 'vae.keras'

features = ['trackPt', 'trackEta', 'trackPhi', 'trackD0Sig', 'trackZ0SinTheta', 'trackQ', 'trackFitQuality', 'trackNIBLHits', 'trackPixHits', 'trackIsSecTrk', 'trackMatchedToVSI','trkNonSignalPhotonConvPtcone30'] # input features 
branches = ['mu', 'EventNumber', 'RunNumber', 'dsid', 'genWeight', 'eventWeight', 'leptonWeight', 'photonWeight', 'JVTWeight', 'pileupWeight', 'trigWeight', 'trigWeight_singleElectronTrig', 'trigWeight_singleMuonTrig', 'trigWeight_singlePhotonTrig', 'trigMatch_metTrig', 'trigMatch_singleElectronTrig', 'trigMatch_singleMuonTrig', 'trigMatch_singlePhotonTrig', 'nEle_base', 'nEle_signal', 'nMu_base', 'nMu_signal', 'nPhoton_base', 'nPhoton_signal', 'hasZCand', 'mll', 'lep1Flavor', 'lep1Q', 'lep1Pt', 'lep1Eta', 'lep1Phi', 'lep2Flavor', 'lep2Q', 'lep2Pt', 'lep2Eta', 'lep2Phi', 'nJet', 'leadJetPt', 'leadJetEta', 'leadJetPhi', 'leadJetM', 'MET', 'MET_invis_electrons', 'MET_invis_muons', 'MET_invis_photons', 'MET_invis_leptons', 'MET_Phi', 'MET_invis_electrons_Phi', 'MET_invis_muons_Phi', 'MET_invis_photons_Phi', 'MET_invis_leptons_Phi', 'MET_Signif', 'MET_invis_electrons_Signif', 'MET_invis_muons_Signif', 'MET_invis_photons_Signif', 'MET_invis_leptons_Signif', 'MET_minDPhiJetMET', 'MET_invis_electrons_minDPhiJetMET', 'MET_invis_muons_minDPhiJetMET', 'MET_invis_photons_minDPhiJetMET', 'MET_invis_leptons_minDPhiJetMET', 'trackPt', 'trackEta', 'trackPhi', 'trackZ0', 'trackD0', 'trackD0Sig', 'trackZ0SinTheta', 'trackQ', 'trackFitQuality', 'trackOrigin', 'trackType', 'trackLabel', 'trackPdgId', 'trackParPdgId', 'trackNIBLHits', 'trackPixHits', 'trackIsBaseMuon', 'trackIsBaseElectron', 'trackIsBasePhoton', 'trackIsSignalMuon', 'trackIsSignalElectron', 'trackIsSignalPhoton', 'trackIsSecTrk', 'trackMatchedToVSI', 'trkNonBaseAssocPtcone20', 'trkNonBaseAssocPtcone30', 'trkNonBaseAssocPtcone40', 'trkNonBasePhotonConvPtcone20', 'trkNonBasePhotonConvPtcone30', 'trkNonBasePhotonConvPtcone40', 'trkNonSignalAssocPtcone20', 'trkNonSignalAssocPtcone30', 'trkNonSignalAssocPtcone40', 'trkNonSignalPhotonConvPtcone20', 'trkNonSignalPhotonConvPtcone30', 'trkNonSignalPhotonConvPtcone40'] # branches to save in the ntuples

weighted_training = False # True or False
lumi = 138950 # only used if weighted_training is True

preselection = "RunNumber>0"

train_over_signal_not_truth_matched = True # True or False. If True, train over all signal tracks. If False, train over all SUSY truth-matched signal tracks.
label_signal_not_truth_matched = '1'  # If train_over_signal_not_truth_matched = True, '0' or '1'

training_tracks = 125000 # How many tracks to keep during training. -1 to train over all tracks, otherwise a value. Make sure you have tracks from all samples.

store_encodeddata = True

def read_samples(samples_to_read = 'training', remove_signal_not_truth_matched = False, label_signal_not_truth_matched = '0'):

    print('Using samples in {} : {}'.format(path, os.listdir(path)))

    ####################################################
    if samples_to_read == 'training':
        print("Reading training samples")

        higgsino_171_170p5_170_file = uproot.open(path+'/signal.root:tree_NoSys;1') 
        Znunu_file                  = uproot.open(path+'/background.root:tree_NoSys;1') 

        #branches = higgsino_171_170p5_170_file.keys()

        df_171_170p5_170 = higgsino_171_170p5_170_file.arrays(branches,preselection,library="pd")
        df_Znunu         = Znunu_file.arrays(branches,preselection,library="pd")

        df_171_170p5_170['Sample'] = '171_170p5_170'
        df_Znunu['Sample']         = 'Znunu'

        print(df_171_170p5_170)
        print(df_Znunu)

        if remove_signal_not_truth_matched == True:
            print("remove_signal_not_truth_matched == True. Removing signal not truth matched tracks.")
            df_171_170p5_170.drop(df_171_170p5_170[(df_171_170p5_170['trkparpdgId'] != 1000023)  & (df_171_170p5_170['trkparpdgId'] != 1000024)].index, inplace=True)
            #df_150p5_150p5_150.drop(df_150p5_150p5_150[(df['trkparpdgId'] != 1000023)  & (df['trkparpdgId'] != 1000024)].index, inplace=True)            
            #df_150p7_150p35_150.drop(df_150p7_150p35_150[(df['trkparpdgId'] != 1000023)  & (df['trkparpdgId'] != 1000024)].index, inplace=True)

        #frames = [df_171_170p5_170, df_150p5_150p5_150, df_150p7_150p35_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]
        frames = [df_171_170p5_170, df_Znunu]

        df = pd.concat(frames)

    ####################################################

    elif samples_to_read == 'other':
        print("Reading other samples")
        higgsino_151_151_150_file = uproot.open(path+'/signals_by_process/N2C1p.root:tree_NoSys;1') 

        #branches = higgsino_151_151_150_file.keys()

        df_151_151_150 = higgsino_151_151_150_file.arrays(branches,preselection,library="pd")

        df_151_151_150['Sample'] = '151_150p5_150'
        df = df_151_151_150

    ####################################################

    # Adding branch weight for weighted training

    if weighted_training == True:
        print("weighted_training set to True. Adding the \"weight\" column with \"weight\"=eventWeight*xsec*138950.")
        df["weight"]= df["eventWeight"]*df["xsec"]*lumi
    else:
        print("weighted_training set to False. Not considering weights in the training.")

    # Defining Ratio
    # df['RatioJetPtMET']= np.full( len(df.index), -1)
    # df['RatioJetPtMET']= df['jetPt']/df['met_Et']


    # Track labelling
    df['Label'] = np.full( len(df.index), -1)
    if (train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='0') or (train_over_signal_not_truth_matched == False):
        df.loc[ (df['trackParPdgId'] != 1000023) & (df['trackParPdgId'] != 1000024), 'Label'] = 0
        df.loc[ (df['trackParPdgId'] == 1000023) | (df['trackParPdgId'] == 1000024), 'Label' ] = 1 
    elif train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='1':
        df.loc[ (df['Sample'] != '171_170p5_170') & (df['Sample'] != '150p5_150p5_150') & (df['Sample'] != '150p7_150p35_150'), 'Label'] = 0  
        df.loc[ (df['Sample'] == '171_170p5_170') | (df['Sample'] == '150p5_150p5_150') | (df['Sample'] == '150p7_150p35_150'), 'Label'] = 1 

    if (  len(df[df['Label'] == -1].index)  != 0 ):
        raise Exception('Errors in associating track labels')
    elif (  len(df[df['Label'] == -1].index)  == 0 ):
        print('All tracks have been labeled')
        print('Labelled 1 tracks = {}, Labelled 0 tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))

    # Dataframe splitting according to EventNumber
    A = df[df['EventNumber']%2 == 0]
    B = df[df['EventNumber']%2 == 1]

    print('Labelled 1 tracks in A = {}, Labelled 0 tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
    print('Labelled 1 tracks in B = {}, Labelled 0 tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))

    print('###### Dataframe df #####')
    print(df)
    print('###### Dataframe A #####')
    print(A)
    print('###### Dataframe B #####')
    print(B)
    return(df,A,B)


def train(df,set,model_name):

    if True:
        # Define your VAE architecture
        latent_dim = 10
        if model_name=='B':
            print(set)
        encoder = tf.keras.Sequential([
            tf.keras.layers.Input(shape=(len(features),)),
            tf.keras.layers.Dense(256, activation='relu'),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dense(latent_dim + latent_dim)  # Two outputs for mean and variance
        ])

        decoder = tf.keras.Sequential([
            tf.keras.layers.Input(shape=(latent_dim,)),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dense(256, activation='relu'),
            tf.keras.layers.Dense(len(features))  # Output layer to reconstruct input
        ])

        def sampling(mean, log_var, latent_dim):
            batch_size = tf.shape(mean)[0] 
            epsilon = tf.random.normal(shape=(batch_size, latent_dim))
            return mean + tf.exp(0.5 * log_var) * epsilon


        # Define VAE model
        inputs = tf.keras.layers.Input(shape=(len(features),))
        mean_log_var = encoder(inputs)
        print('mean_log_var.shape',mean_log_var.shape)
        mean, log_var = tf.split(mean_log_var, num_or_size_splits=2, axis=-1)
        z = sampling(mean, log_var, latent_dim)
        print('z.shape',z.shape)
        outputs = decoder(z)
        vae = tf.keras.Model(inputs, outputs)

        # Define the loss function for VAE
        kl_loss = -0.5 * tf.keras.backend.sum(1 + mean_log_var[:, latent_dim:] - tf.keras.backend.square(mean_log_var[:, :latent_dim]) -tf.keras.backend.exp(mean_log_var[:, latent_dim:]), axis=-1)
        reconstruction_loss = tf.keras.losses.mean_squared_error(inputs, outputs)
        vae_loss = tf.keras.backend.mean(reconstruction_loss + kl_loss)

        vae.add_loss(vae_loss)

        # Compile the VAE model
        vae.compile(optimizer='adam')

        #vae.fit(np.asarray(set[features]), np.asarray(set['Label']), epochs=1)
        # training_set used only in training to keep as many signal than background events in training
        training_set = pd.concat([set[set['Sample']=='171_170p5_170'][:100], set[set['Sample']=='Znunu'][:100]])
        vae.fit(np.asarray(training_set[features]), np.asarray(training_set['Label']), epochs=1)
        vae.save(model_name+'.keras')
        
        encoded_data = encoder.predict(np.asarray(set[features]))
        encoder.summary()
        print(f'encoded_data = {encoded_data}, shape = {encoded_data.shape}')
        mean_data, log_var_data = tf.split(encoded_data, num_or_size_splits=2, axis=-1)
        z_data = sampling(mean_data, log_var_data, latent_dim) 
        predictions = decoder.predict(z_data)
        decoder.summary()
        print(f'predictions = {predictions}, shape = {predictions.shape}')
        kl_loss = -0.5 * tf.keras.backend.sum(1 + encoded_data[:, latent_dim:] - tf.keras.backend.square(encoded_data[:, :latent_dim]) -tf.keras.backend.exp(encoded_data[:, latent_dim:]), axis=-1)
        print(f'Kullback-Leibler Divergence function = {kl_loss}')
        reconstruction_loss = tf.keras.losses.mean_squared_error(np.asarray(set[features]), predictions)
        print(f'p(x|z) = {reconstruction_loss}')
        vae_loss = reconstruction_loss + kl_loss
        print(f'VAE loss p(x|z) + KL = {vae_loss}')
        
        
        set_decoded_data = pd.DataFrame(predictions)
        set_decoded_data.columns = features
        set_decoded_data['Sample'] = np.array(set['Sample'])
        set_decoded_data.set_index(set.index, inplace=True)
        print('set_decoded_data = ',set_decoded_data)
            
        return vae_loss,set_decoded_data

def save_trees_original_data_withVAE(A,B, vae_loss_A, vae_loss_B):

    A['VAE_loss'] = vae_loss_A
    B['VAE_loss'] = vae_loss_B

    tested_df = pd.concat([A,B])

    outputdir_name = 'output_withVAE'
    os.system('mkdir {}'.format(outputdir_name))

    samples = tested_df['Sample'].unique()

    branches = tested_df.keys().to_list()
    branches.remove('Sample') # Sample branch should not be added on output trees

    print("Dataframe contaning: {} samples".format(samples))
    print("Dataframe contaning: {} branches".format(branches))

    for sample in samples:
        print("Processing: {} sample".format(sample))
        distributions_dict={}

        for branch in branches: 
            distributions_dict[branch] = np.array(tested_df[tested_df['Sample']==sample][branch])
            
        with uproot.recreate("{}/{}.root".format(outputdir_name,sample)) as f:
            f[sample] = distributions_dict

def save_trees_decoded_data(decoded_data_A,decoded_data_B):

    decoded_data_df = pd.concat([decoded_data_A,decoded_data_B])

    outputdir_name = 'output_encodedVAE'
    os.system('mkdir {}'.format(outputdir_name))
    samples = decoded_data_df['Sample'].unique()

    branches = decoded_data_df.keys().to_list()
    branches.remove('Sample') # Sample branch should not be added on output trees

    print("Dataframe contaning: {} samples".format(samples))
    print("Dataframe contaning: {} branches".format(branches))

    for sample in samples:
        print("Processing: {} sample".format(sample))
        distributions_dict={}

        for branch in branches: 
            distributions_dict[branch] = np.array(decoded_data_df[decoded_data_df['Sample']==sample][branch])
            
        with uproot.recreate("{}/{}.root".format(outputdir_name,sample)) as f:
            f[sample] = distributions_dict
         

def main():
    df,A,B = read_samples('training' , False, '1')
    vae_loss_A,decoded_data_A = train(df,A,'A')
    vae_loss_B,decoded_data_B = train(df,B,'B')
    save_trees_original_data_withVAE(A,B,vae_loss_A,vae_loss_B)
    save_trees_decoded_data(decoded_data_A,decoded_data_B)
if __name__ == "__main__":
    main()
