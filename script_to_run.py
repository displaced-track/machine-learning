#!/usr/bin/python
import os

os.system('source ~/venv-ml/bin/activate')
os.system('source /cvmfs/sft.cern.ch/lcg/views/LCG_105_cuda/x86_64-el9-gcc11-opt/setup.sh')
os.system('cd /afs/cern.ch/work/e/eballabe/public/machine-learning')
os.system('python3 /afs/cern.ch/work/e/eballabe/public/machine-learning/Train.py')
