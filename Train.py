import uproot
import os
import os.path
os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

print('Using tf version {}'.format(tf.__version__))

path = '/eos/user/a/alsala/ntuples/R24/FlatNtuples'

saved_model_A = '/afs/cern.ch/work/e/eballabe/public/machine-learning/DNN_A.keras'
saved_model_B = '/afs/cern.ch/work/e/eballabe/public/machine-learning/DNN_B.keras'

features = ['trackPt', 'trackEta', 'trackPhi', 'trackD0', 'trackD0Sig', 'trackZ0SinTheta', 'trackFitQuality', 'trackNIBLHits', 'trackPixHits','trkNonSignalPhotonConvPtcone30', 'trackMt', 'trackPixeldEdx', 'trackTRTdEdx'] # input features 
branches = ['mu', 'EventNumber', 'RunNumber', 'dsid', 'genWeight', 'eventWeight', 'leptonWeight', 'photonWeight', 'JVTWeight', 'pileupWeight', 'trigWeight', 'trigWeight_singleElectronTrig', 'trigWeight_singleMuonTrig', 'trigWeight_singlePhotonTrig', 'trigMatch_metTrig', 'trigMatch_singleElectronTrig', 'trigMatch_singleMuonTrig', 'trigMatch_singlePhotonTrig', 'mll', 'nJet', 'leadJetPt', 'leadJetEta', 'leadJetPhi', 'leadJetM', 'MET', 'MET_invis_electrons', 'MET_invis_muons', 'MET_invis_photons', 'MET_invis_leptons', 'MET_Phi', 'MET_invis_electrons_Phi', 'MET_invis_muons_Phi', 'MET_invis_photons_Phi', 'MET_invis_leptons_Phi', 'MET_Signif', 'MET_invis_electrons_Signif', 'MET_invis_muons_Signif', 'MET_invis_photons_Signif', 'MET_invis_leptons_Signif', 'MET_minDPhiJetMET', 'MET_invis_electrons_minDPhiJetMET', 'MET_invis_muons_minDPhiJetMET', 'MET_invis_photons_minDPhiJetMET', 'MET_invis_leptons_minDPhiJetMET', 'trackPt', 'trackEta', 'trackPhi', 'trackZ0', 'trackD0', 'trackD0Sig', 'trackZ0SinTheta', 'trackQ', 'trackFitQuality', 'trackOrigin', 'trackType', 'trackLabel', 'trackPdgId', 'trackParPdgId', 'trackNIBLHits', 'trackPixHits', 'trackIsSecTrk', 'trackMatchedToVSI', 'trkNonBaseAssocPtcone20', 'trkNonBaseAssocPtcone30', 'trkNonBaseAssocPtcone40', 'trkNonBasePhotonConvPtcone20', 'trkNonBasePhotonConvPtcone30', 'trkNonBasePhotonConvPtcone40', 'trkNonSignalAssocPtcone20', 'trkNonSignalAssocPtcone30', 'trkNonSignalAssocPtcone40', 'trkNonSignalPhotonConvPtcone20', 'trkNonSignalPhotonConvPtcone30', 'trkNonSignalPhotonConvPtcone40','trackMt', 'trackPixeldEdx', 'trackTRTdEdx'] # branches to save in the ntuples

weighted_training = False # True or False
lumi = 138950 # only used if weighted_training is True

preselection = "RunNumber>0"

train_over_signal_not_truth_matched = True # True or False. If True, train over all signal tracks. If False, train over all SUSY truth-matched signal tracks.
label_signal_not_truth_matched = '1'  # If train_over_signal_not_truth_matched = True, '0' or '1'

reduce_samples = True # If True, train over a subset of samples
reduce_value = 30000 # How many tracks to keep

def read_samples(samples_to_read = 'training', remove_signal_not_truth_matched = False, label_signal_not_truth_matched = '0', read = 'standard'):

    print('Using samples in {} : {}'.format(path, os.listdir(path)))

    ####################################################
    if samples_to_read == 'training':
        print("Reading training samples")

        #higgsino_176_175p5_175_Run2_file = uproot.open(path+'/higgsino_176_175p5_175_Run2.root:tree_NoSys;1') 
        higgsino_176_175p5_175_Run3_file = uproot.open(path+'/higgsino_176_175p5_175_Run3.root:tree_NoSys;1') 
        Znunu_file                  = uproot.open(path+'/Znunu.root:tree_NoSys;1') 
        
        #branches = higgsino_171_170p5_170_file.keys()
        
        #df_176_175p5_175_Run2 = higgsino_176_175p5_175_Run2_file.arrays(branches,preselection,library="pd")
        df_176_175p5_175_Run3 = higgsino_176_175p5_175_Run3_file.arrays(branches,preselection,library="pd")
        df_Znunu              = Znunu_file.arrays(branches,preselection,library="pd")
        
        #df_176_175p5_175_Run2['Sample'] = '176_175p5_175'
        df_176_175p5_175_Run3['Sample'] = '176_175p5_175'
        df_Znunu['Sample']         = 'Znunu'
        if read == 'standard' and reduce_samples == True:
            df_176_175p5_175_Run3 = df_176_175p5_175_Run3.iloc[:reduce_value]
            df_Znunu = df_Znunu.iloc[:reduce_value]

        #print("df_176_175p5_175_Run2 = ", df_176_175p5_175_Run2)
        print("df_176_175p5_175_Run3 = ", df_176_175p5_175_Run3)
        print("df_Znunu = ", df_Znunu)
        
        if remove_signal_not_truth_matched == True:
            print("remove_signal_not_truth_matched == True. Removing signal not truth matched tracks.")
            df_176_175p5_175_Run2.drop(df_176_175p5_175_Run2[(df_176_175p5_175_Run2['trkparpdgId'] != 1000023)  & (df_176_175p5_175_Run2['trkparpdgId'] != 1000024)].index, inplace=True)
            df_176_175p5_175_Run3.drop(df_176_175p5_175_Run3[(df_176_175p5_175_Run3['trkparpdgId'] != 1000023)  & (df_176_175p5_175_Run3['trkparpdgId'] != 1000024)].index, inplace=True)
            
        #frames = [df_171_170p5_170, df_150p5_150p5_150, df_150p7_150p35_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]
        frames = [df_176_175p5_175_Run3, df_Znunu]
        
        df = pd.concat(frames)

    ####################################################

    elif samples_to_read == 'other':
        print("Reading other samples")
        higgsino_151_151_150_file = uproot.open(path+'/signals_by_process/N2C1p.root:tree_NoSys;1') 
        
        #branches = higgsino_151_151_150_file.keys()
        
        df_151_151_150 = higgsino_151_151_150_file.arrays(branches,preselection,library="pd")
        
        df_151_151_150['Sample'] = '151_150p5_150'
                
        df = df_151_151_150

    ####################################################

    # Adding branch weight for weighted training

    if weighted_training == True:
        print("weighted_training set to True. Adding the \"weight\" column with \"weight\"=eventWeight*xsec*138950.")
        df["weight"]= df["eventWeight"]*df["xsec"]*lumi
    else:
        print("weighted_training set to False. Not considering weights in the training.")
    # Defining Ratio
    # df['RatioJetPtMET']= np.full( len(df.index), -1)
    # df['RatioJetPtMET']= df['jetPt']/df['met_Et']

    # Track labelling
    df['Label'] = np.full( len(df.index), -1)
    if (train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='0') or (train_over_signal_not_truth_matched == False):
        df.loc[ (df['trackParPdgId'] != 1000023) & (df['trackParPdgId'] != 1000024), 'Label'] = 0
        df.loc[ (df['trackParPdgId'] == 1000023) | (df['trackParPdgId'] == 1000024), 'Label' ] = 1 
    elif train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='1':
        df.loc[ (df['Sample'] != '176_175p5_175') & (df['Sample'] != '150p5_150p5_150') & (df['Sample'] != '150p7_150p35_150'), 'Label'] = 0  
        df.loc[ (df['Sample'] == '176_175p5_175') | (df['Sample'] == '150p5_150p5_150') | (df['Sample'] == '150p7_150p35_150'), 'Label'] = 1 
    
    if (  len(df[df['Label'] == -1].index)  != 0 ):
        raise Exception('Errors in associating track labels')
    elif (  len(df[df['Label'] == -1].index)  == 0 ):
        print('All tracks have been labeled')
        print('Labelled 1 tracks = {}, Labelled 0 tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))

    # Dataframe splitting according to EventNumber
    A = df[df['EventNumber']%2 == 0]
    B = df[df['EventNumber']%2 == 1]

    print('Labelled 1 tracks in A = {}, Labelled 0 tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
    print('Labelled 1 tracks in B = {}, Labelled 0 tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))

    print('###### Dataframe df #####')
    print("df = ", df)
    print('###### Dataframe A #####')
    print("A = ", A)
    print('###### Dataframe B #####')
    print("B = ", B)
    return(df,A,B)


def train_A(df,A):

    import datetime
    if os.path.isfile(saved_model_A) is True:
        print ('Model already exists')
        model_A = tf.keras.models.load_model(saved_model_A)

    elif os.path.isfile(saved_model_A) is False:

        model_A = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_A.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        if weighted_training == True:
            history = model_A.fit(np.asarray(A[features]), np.asarray(A['Label']), epochs=5, sample_weight=A['weight'], callbacks=[tensorboard_callback])
        else:
            history = model_A.fit(np.asarray(A[features]), np.asarray(A['Label']), epochs=5, callbacks=[tensorboard_callback])
        model_A.save(saved_model_A)

    print(model_A.summary())
    print(model_A.history)
    return model_A


def train_B(df,B):

    import datetime
    if os.path.isfile(saved_model_B) is True:
        print ('Model already exists')
        model_B = tf.keras.models.load_model(saved_model_B)

    elif os.path.isfile(saved_model_B) is False:

        model_B = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_B.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        if weighted_training == True:
            history = model_B.fit(B[features], B['Label'], epochs=5, sample_weight=B['weight'], callbacks=[tensorboard_callback])
        else:
            history = model_B.fit(B[features], B['Label'], epochs=5, callbacks=[tensorboard_callback])
        model_B.save(saved_model_B)

    print(model_B.summary())
    print(model_B.history)
    return model_B


def test(df,A,B):

    model_A = tf.keras.models.load_model(saved_model_A)
    model_B = tf.keras.models.load_model(saved_model_B)

    print("Proceeding to predict A")
    predictions_A = model_B.predict(A[features])

    print("Proceeding to predict B")
    predictions_B = model_A.predict(B[features])

    A['DNN'] = predictions_A
    B['DNN'] = predictions_B

    tested_df = pd.concat([A,B])
    #tested_df.to_pickle('tested_df.pickle')
    return tested_df


def save_tree(tested_df):
    import os
    #test_set = pd.read_pickle('tested_df.pickle')

    outputdir_name = 'output_withDNN'
    os.system('mkdir {}'.format(outputdir_name))

    samples = tested_df['Sample'].unique()

    branches = tested_df.keys().to_list()
    branches.remove('Sample') # Sample branch should not be added on output trees

    print("Dataframe contaning: {} samples".format(samples))
    print("Dataframe contaning: {} branches".format(branches))

    for sample in samples:
        print("Processing: {} sample".format(sample))
        distributions_dict={}

        for branch in branches: 
            distributions_dict[branch] = np.array(tested_df[tested_df['Sample']==sample][branch])
            
        # No need to set types anymore in uproot. To change them, you can do
        # distributions_dict["EventNumber"] = distributions_dict["EventNumber"].astype('int32')
        # distributions_dict["DNN"] = distributions_dict["DNN"].astype('float32')
            
        with uproot.recreate("{}/{}.root".format(outputdir_name,sample)) as f:
            # without distributions_dict is: f[sample] = tested_df[tested_df['Sample']==sample]  
            f[sample] = distributions_dict
            
def main():
    
    if train_over_signal_not_truth_matched == True:
        print("train_over_signal_not_truth_matched set to True.")
        if label_signal_not_truth_matched == '0':
            print("Signal not truth matched tracks will be considered as background tracks having trkparpdgId != 1000023 or 1000024. A Label 0 will be associated to them.")
            df,A,B = read_samples('training' , False, '0')
        elif label_signal_not_truth_matched == '1':
            print("Signal not truth matched tracks will be considered as signal tracks. A Label 1 will be associated to them as for the signal truth matched tracks")
            df,A,B = read_samples('training' , False, '1')
    elif train_over_signal_not_truth_matched == False:
        print("train_over_signal_not_truth_matched set to False. Removing Signal not truth matched tracks from the training.")
        df,A,B = read_samples('training' , True)
    
    print("Proceeding to Train A")
    train_A(df,A)
    print("Proceeding to Train B")
    train_B(df,B)

    if train_over_signal_not_truth_matched == False:
        print("train_over_signal_not_truth_matched set to False. Re-reading all training samples to include in the test set the signal not truth matched tracks removed before.")
        df,A,B = read_samples('training', read='full')

    if reduce_samples == True:
        print("reduce_samples set to True. Part of the samples were not used for the training. Re-reading all training samples to include them in the test set.")
        df,A,B = read_samples('training', read='full')
        
    print("Proceeding to test trained samples")
    tested_df = test(df,A,B)
    print("Proceeding to save trees from tested samples")
    print(tested_df)
    save_tree(tested_df)
    '''
    df,A,B = read_samples('other')
    print("Proceeding to test the other samples")
    tested_df = test(df,A,B)
    print("Proceeding to save trees from the other samples")
    save_tree(tested_df)
    '''

if __name__ == "__main__":
    main()

